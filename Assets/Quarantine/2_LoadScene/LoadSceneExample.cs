﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class LoadSceneExample : MonoBehaviour {

	public string AssetBundleName;
	public string AssetName = "TestScene";

	void Start() {
		var myLoadedAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, AssetBundleName));
		if (myLoadedAssetBundle == null) {
			Debug.Log("Failed to load AssetBundle!");
			return;
		}

		string[] scenes = myLoadedAssetBundle.GetAllScenePaths ();
		if(scenes.Length != 0)
		{
			string sceneName = Path.GetFileNameWithoutExtension (scenes [0]);
			SceneManager.LoadScene (sceneName);
		}
	}
}
