﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Example : MonoBehaviour {

    [Header("Assets")]
    [SerializeField]
    TextAsset textAsset = null;

    [SerializeField]
    ColorAsset colorAsset = null;

    [Header("Main")]
    [SerializeField]
    Text uiText = null;

    void Start ()
    {
        this.uiText.color = colorAsset.MainColor;
        this.uiText.text = textAsset.text;
	}
}
