﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class Loader : MonoBehaviour {

	public string BundleName;
    public enum Theme
    {
        Spring,
        Fall,
    }

	public Theme loadTheme = Theme.Fall;

    IEnumerator Start()
    {
		//Load Variants
		var ab = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, BundleName + "." + loadTheme.ToString().ToLower()));
       
		//buat debug doang
		foreach (var assetName in ab.GetAllAssetNames())
        {
            Debug.LogFormat("Assets: {0}", assetName);
        }

		//Load Scene "Level" from bundle
		AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "level"));
        yield return SceneManager.LoadSceneAsync("Level", LoadSceneMode.Additive);
    }
	
}
