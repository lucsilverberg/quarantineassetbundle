﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LoadAssetBundle : MonoBehaviour {

	public string AssetBundleName;
	public string AssetName = "MyObject";

	void Start() {
		var myLoadedAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, AssetBundleName));
		if (myLoadedAssetBundle == null) {
			Debug.Log("Failed to load AssetBundle!");
			return;
		}

		var prefab = myLoadedAssetBundle.LoadAsset<GameObject>(AssetName);
		Instantiate(prefab);
	}
}
